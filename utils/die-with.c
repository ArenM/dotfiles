#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/pidfd.h>
#include <unistd.h>
#include <assert.h>

int main(int argc, char *argv[]) {
  assert(argc >= 3);

  // get parent pid
  int parent_pid;
  if (strcmp(argv[1], "-") == 0) {
    parent_pid = getpid();
  } else {
    parent_pid = strtoul(argv[1], NULL, 0);
  }

  int parent = pidfd_open(parent_pid, 0);
  if (parent == -1) {
    perror("failed to find parent");
    exit(1);
  }

  // start process
  pid_t child_pid = fork();
  if (child_pid == 0) {
    execvp(argv[2], &argv[2]);
  }
  int child = pidfd_open(child_pid, 0);
  if (child == -1) {
    perror("failed to get child");
  }

  // poll child pid & parent pid
  struct pollfd fds[2] = {
    { .fd = parent, .events = POLLIN },
    { .fd = child, .events = POLLIN },
  };
  int ret = poll(fds, 2, -1);

  // either the child or parent exited, kill the child for good measure (and simplicity)
  pidfd_send_signal(child, SIGTERM, NULL, 0);
}
