local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local conf = require("telescope.config").values
local actions = require("telescope.actions")
local action_state = require("telescope.actions.state")

email_snippet = function(opts)
  opts = opts or {}
  pickers.new({}, {
    prompt_title = "snippets",
    finder = finders.new_table {
      results = {
        "X-Sourcehut-Patchset-Update: PROPOSED",
        "X-Sourcehut-Patchset-Update: NEEDS_REVISION",
        "X-Sourcehut-Patchset-Update: SUPERSEDED",
        "X-Sourcehut-Patchset-Update: APPROVED",
        "X-Sourcehut-Patchset-Update: REJECTED",
        "X-Sourcehut-Patchset-Update: APPLIED",
        "Signed-off-by: Aren Moynihan <aren@peacevolution.org>",
        "Co-developed-by: Aren Moynihan <aren@peacevolution.org>",
        "Acked-by: Aren Moynihan <aren@peacevolution.org>",
        "Tested-by: Aren Moynihan <aren@peacevolution.org>",
        "Reviewed-by: Aren Moynihan <aren@peacevolution.org>",
        "Reported-by: ",
        "Suggested-by: ",
      },
    },
    sorter = conf.generic_sorter(nil),
    attach_mappings = function(prompt_bufnr, map)
      actions.select_default:replace(function()
        actions.close(prompt_bufnr)
        local sel = action_state.get_selected_entry()
        vim.api.nvim_put({sel[1]}, "l", true, false)
      end)
      return true
    end
  }):find()
end

vim.keymap.set('n', '<leader>s', email_snippet)
