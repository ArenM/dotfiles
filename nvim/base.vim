"
" Basic settings that apply to any vim session, no dependencies on plugsin
"

set nocompatible

" dont discard buffers when switching to a different one
set hidden

" configure nice defaults
syntax enable
filetype plugin on
set number
set wildmode=longest:full
set wildmenu
set relativenumber
set ignorecase
set smartcase
set timeoutlen=300
set mouse=

set shiftwidth=2
set expandtab
set smarttab
set smartindent

set textwidth=80
set colorcolumn=85
" wrap is disabled by default, but configure it to be nicer when it is enabled
set nowrap
set linebreak
set breakindent

set inccommand=split

" set the window title
set title

" Useful for working on syntax highlighting
" Call to get the syntax types under the cursor
function! SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

" show whitespace characters
" Possible nbsp characters: ☠, ?, ⍽, ▆, ⛛
function CustomHilights()
  match TrailingSpace /[[:space:]]\+$/

  hi TrailingSpace guibg=Red ctermbg=Red
  hi clear SpellCap " More often than not this is intentional
endfunction

set list
set listchars=eol:¬,tab:<·>,multispace:␣,lead:·,trail:~,extends:>,precedes:<,nbsp:
autocmd ColorScheme * call CustomHilights()

" Font for when neovim is running in a gui
set guifont=InputMono

"
" Basic key bindings which don't require plugins
"

" Treat movement in wrapped lines like moving in normal lines
" TODO: make moving [count] lines skip this
" Perhaps see cpo-n
noremap k gk
noremap j gj

" Extra movement actions (mostly from kakoune)
noremap gl $
noremap gh ^
noremap gH 0
noremap gk gg
noremap gj G
nnoremap U <c-r>

" Basic keybinds which don't require plugins
map <Space> <leader>

" Get out of terminal mode a bit easier
tnoremap <esc>      <c-\><c-n>
tnoremap <s-esc>    <esc>

" Clear search highlight
noremap <leader>ch <cmd>noh<cr>

" Enable soft word wrap instead fo inserting EOLs
function ToggleWW()
  if &l:wrap " non zero -- disable
    set nowrap
    set formatoptions+=tc
  else "zero -- enable
    set wrap
    set formatoptions-=tc
  endif
endfunction
map <leader>w <cmd>call ToggleWW()<CR>

" Buffer switching
noremap <Tab> :bnext<CR>
noremap <S-Tab> :bprev<CR>
noremap <bs> <c-o>

" Easier window switching
noremap <a-h> <cmd>wincmd h<CR>
noremap <a-j> <cmd>wincmd j<CR>
noremap <a-k> <cmd>wincmd k<CR>
noremap <a-l> <cmd>wincmd l<CR>

tnoremap <a-h> <cmd>wincmd h<CR>
tnoremap <a-j> <cmd>wincmd j<CR>
tnoremap <a-k> <cmd>wincmd k<CR>
tnoremap <a-l> <cmd>wincmd l<CR>

noremap sh :set nosplitright<CR>:vsplit<CR>:set splitright<CR>
noremap sj :set splitbelow<CR>:split<CR>
noremap sk :set nosplitbelow<CR>:split<CR>:set splitbelow<CR>
noremap sl :set splitright<CR>:vsplit<CR>

" Yank & Paste mappings
noremap Y y$
noremap <Leader>y "+y
noremap <Leader>p "+p

" Save undo history to a file
" NOTE: I have plenty of resources to spare on my computer, but this could
"       potentially cause performance issues
set undodir=~/.local/share/nvim/undo
set undofile
set undolevels=10000
set undoreload=100000

"
" Special filetype settings
"
function ConfigureEmail()
  setlocal textwidth=72
  setlocal commentstring=>\ %s
  setlocal colorcolumn=73
endfunction

autocmd FileType gitcommit,mail,markdown,vimwiki,eml setlocal spell
autocmd FileType gitcommit,mail,eml call ConfigureEmail()
autocmd FileType zig setlocal commentstring=//\ %s

"
" Folding styles
"

" Setting fillchars/fold to a space. It's there you just can't see it.
set fillchars=fold:\  
set fillchars+=vert:\│
set foldtext=CleanFoldText()
autocmd FileType rust if FoldCondition() | call EnableFolding() | endif

function FoldCondition()
  return line('$') > 150
endfunction

function EnableFolding()
  set foldmethod=expr
  set foldexpr=nvim_treesitter#foldexpr()
  setlocal foldlevel=0
  setlocal foldcolumn=1
endfunction

function CleanFoldText()
  " Copied from https://github.com/pseewald/vim-anyfold/blob/master/autoload/anyfold.vim
  let fs = v:foldstart
  while getline(fs) !~ '\w'
    let fs = nextnonblank(fs + 1)
  endwhile
  if fs > v:foldend
    let line = getline(v:foldstart)
  else
    let line = substitute(getline(fs), '\t', repeat(' ', &tabstop), 'g')
  endif

  let foldSize = 1 + v:foldend - v:foldstart
  let foldSizeStr = " " . substitute("%s lines", "%s", string(foldSize), "g") . " "
  let foldLevelStr = repeat(" + ", v:foldlevel)
  let lineCount = line("$")

  " Subtracting 10 to shift the text over a bit, so that I don't have to calulate the width of the buffer
  let w    = winwidth(0) - &foldcolumn - &number * &numberwidth - strwidth(line.foldSizeStr.foldLevelStr) - 10
  let wmax = 85 - strwidth(line)
  let expansionString = repeat(" ", min([w, wmax]))

  return line.expansionString.foldSizeStr.foldLevelStr
endfunction

function! EditNotes()
  cd ~/Nextcloud/personal-wiki
  edit ~/Nextcloud/personal-wiki/index.md
endfunc

map <silent> <leader>ww <cmd>call EditNotes()<CR>

" TODO: it might be fun to handle fils with spaces
autocmd filetype vimwiki,markdown setlocal suffixesadd=.wiki,.md
