vim.loader.enable()

-- NOTE: This may break in some terminals, maybe add a check later
-- TODO: I don't feel like translating this to lua, maye do this later
vim.cmd [[if has('termguicolors')
  set termguicolors
endif]]

local colorscheme_plugins = {
  catppuccin = { "catppuccin/nvim", as = "catppuccin", configure = function()
    require("catppuccin").setup({
      background = {
        light = "latte",
        dark = "mocha",
      },
      integrations = {
        cmp = true,
        gitsigns = true,
        treesitter = true,
        harpoon = true,
      }
    })

    vim.cmd.colorscheme('catppuccin')
  end },

  everforest = { 'sainnhe/everforest', configure = function()
    vim.o.background = 'dark'
    vim.g.everforest_background = 'hard'
    vim.g.everforest_better_performance = 1
    vim.g.lightline = { colorscheme = 'everforest' }

    vim.cmd.colorscheme('everforest')
  end },

  sonokai = { 'sainnhe/sonokai', configure = function()
    vim.g.sonokai_style = 'atlantis'
    vim.g.sonokai_better_performance = 1

    vim.cmd.colorscheme('sonokai')
  end },

  base16 = { 'chriskempson/base16-vim', configure = [[
    colorscheme base16-<%= $THEME %>
  ]] }
}


local plugins = {
  colorscheme_plugins.catppuccin,

  -- Plug 'ActivityWatch/aw-watcher-vim'

  -- Git
  'tpope/vim-fugitive',

  -- Dependcies
  'nvim-lua/popup.nvim',
  'nvim-lua/plenary.nvim',

  -- Plugins I'm working on
  '~/dev/nvim/gtk-doc.nvim',

  -- Use nvim in firefox
  { 'glacambre/firenvim', build = ":call firenvim#install(0)", configure = function()
    vim.g.firenvim_config = {
      localSettings = {
        ['https://mas.to'] = {
          takeover = 'never'
        }
      }
    }
  end },

  -- Utilities
  -- Note to self, there's a generic versin which does't support telescope
  'nvim-telescope/telescope-hop.nvim',
  'nvim-telescope/telescope-ui-select.nvim',
  { 'nvim-telescope/telescope.nvim', configure = function()
    local map = vim.keymap.set
    local builtin = require("telescope.builtin")
    local telescope = require 'telescope'

    map('n', '<C-f>', builtin.find_files, {})
    -- noremap <Leader>b <cmd>:lua telescope_buffer_hints<CR>
    map('n', '<Leader>b', builtin.buffers, {})
    map('n', '<Leader>g', builtin.live_grep, {})
    map('n', 'g*', function() builtin.grep_string({search = vim.fn.expand("<cword>")}) end, {})
    map('n', '<Leader>q', builtin.quickfix, {})
    map('n', '<Leader>gs', builtin.git_status, {})
    map('n', '<leader>gb', builtin.git_branches, {})

    vim.g.dashboard_default_executive = 'telescope'

    telescope.setup{
      -- TODO: This isn't quite right, I should impliment the theme myself
      defaults = require("telescope.themes").get_ivy {
        file_sorter = require'telescope.sorters'.get_fzy_sorter,
        generic_sorter = require'telescope.sorters'.get_generic_fzy_sorter,
        mappings = {
          i = {
            ["<c-g>"] = require("telescope").extensions.hop.hop,
            ["<c-f>"] = require("telescope").extensions.hop.hop,
            ["<c-s>"] = require("telescope").extensions.hop.hop_toggle_selection,
          },
          n = {
            ["g"] = require("telescope").extensions.hop.hop,
            ["f"] = require("telescope").extensions.hop.hop,
            ["s"] = require("telescope").extensions.hop.hop_toggle_selection,
            ["<c-q>"] = require("telescope.actions").smart_send_to_qflist,
          },
        },
      },
      pickers = {
        buffers = {
          theme = "ivy",
          sort_lastused = true,
          mappings = {
            i = {
              ["<c-x>"] = "delete_buffer",
            },
            n = {
              ["x"] = "delete_buffer",
              ["dd"] = "delete_buffer",
              ["f"] = require("telescope").extensions.hop.hop,
            },
          },
        },
      },
      extensions = {
        hop = {},
      }
    }

    telescope.load_extension('hop')
    telescope.load_extension('ui-select')

    -- telescope.load_extention('fzy_native')
  end },

  { 'https://github.com/lewis6991/gitsigns.nvim', configure = function()
    require'gitsigns'.setup {
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns
        local opts = { noremap=true, silent=true }

        -- local function map(mode, l, r, opts)
        --   opts.buffer = bufnr
        --   vim.keymap.set(mode, l, r, opts)
        -- end

        local function map(mode, lhs, rhs, opts)
          opts = opts or { noremap=true, silent=true }
          vim.api.nvim_buf_set_keymap(bufnr, mode, lhs, rhs, opts)
        end

        -- Navigation
        map('n', ']c', "&diff ? ']c' : '<cmd>Gitsigns next_hunk<CR>'", {expr=true})
        map('n', '[c', "&diff ? '[c' : '<cmd>Gitsigns prev_hunk<CR>'", {expr=true})

        -- Actions
        map('n', '<leader>hs', '<cmd>Gitsigns stage_hunk<CR>')
        map('v', '<leader>hs', '<cmd>\'<,\'>Gitsigns stage_hunk<CR>')
        map('n', '<leader>hr', '<cmd>Gitsigns reset_hunk<CR>')
        map('v', '<leader>hr', '<cmd>\'<,\'>Gitsigns reset_hunk<CR>')
        map('n', '<leader>hu', '<cmd>Gitsigns undo_stage_hunk<CR>')
        map('n', '<leader>hp', '<cmd>Gitsigns preview_hunk<CR>')
        map('n', '<leader>hb', '<cmd>Gitsigns blame_line<CR>')
        map('n', '<leader>tb', '<cmd>Gitsigns toggle_current_line_blame<CR>')
        map('n', '<leader>hd', '<cmd>Gitsigns diffthis<CR>')
        map('n', '<leader>td', '<cmd>Gitsigns toggle_deleted<CR>')
      end
    }
  end },

  { 'ThePrimeagen/harpoon', configure = function()
    local map = vim.keymap.set
    local mark = require("harpoon.mark")
    local ui = require("harpoon.ui")
    local term = require("harpoon.term")

    local nav_file = function(n) return function()
      ui.nav_file(n)
    end end

    local nav_term = function(n) return function()
      term.gotoTerminal(n)
    end end

    map('n', '<leader>a', mark.add_file, {})
    map('n', '<c-h>', ui.toggle_quick_menu, {})

    -- Mark mappins for buffers
    map("n", "'a", nav_file(1))
    map("n", "'s", nav_file(2))
    map("n", "'d", nav_file(3))
    map("n", "'f", nav_file(4))
    map("n", "'g", nav_file(5))

    -- Semicolon mapings for terminal
    map("n", ";;a", nav_term(1))
    map("n", ";;s", nav_term(2))
    map("n", ";;d", nav_term(3))
    map("n", ";;f", nav_term(4))
    map("n", ";;g", nav_term(5))

    -- noremap ;;ra <cmd>lua require("harpoon.term").sendCommand(1, 1)<cr>
    -- noremap ;;rs <cmd>lua require("harpoon.term").sendCommand(1, 2)<cr>
  end },

  -- Might be useful for lsp diagnostics in the future
  -- 'folke/trouble.nvim',

  -- Looks and data sources
  { 'neovim/nvim-lspconfig', configure = function()
    local nvim_lsp = require('lspconfig')

    local on_attach = function(client, bufnr)
      require'lsp_signature'.on_attach()
      local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
      local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

      --Enable completion triggered by <c-x><c-o>
      -- buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

      -- Mappings.
      local opts = { noremap=true, silent=true }

      -- Use LSP as the handler for formatexpr.
      vim.api.nvim_buf_set_option(0, 'formatexpr', 'v:lua.vim.lsp.formatexpr()')

      -- See `:help vim.lsp.*` for documentation on any of the below functions
      -- TODO: replace with gbrlsnchs/telescope-lsp-handlers.nvim
      buf_set_keymap('n', 'gd',         '<Cmd>Telescope lsp_definitions<CR>', opts)
      buf_set_keymap('n', 'gi',         '<cmd>Telescope lsp_implementations<CR>', opts)
      vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, opts)
      vim.keymap.set('n', '<leader>cr', vim.lsp.buf.code_action, opts)
      buf_set_keymap('n', 'gr',         '<cmd>Telescope lsp_references<CR>', opts)
      buf_set_keymap('n', 'gD',         '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
      buf_set_keymap('n', 'K',          '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
      buf_set_keymap('n', '<C-k>',      '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
      buf_set_keymap('n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
      buf_set_keymap('n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
      buf_set_keymap('n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
      buf_set_keymap('n', '<leader>D',  '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
      vim.keymap.set('n', '<leader>e',  vim.diagnostic.open_float, opts)
      buf_set_keymap('n', '[d',         '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
      buf_set_keymap('n', ']d',         '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
      buf_set_keymap('n', '<leader>q',  '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
      vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, opts)
      vim.keymap.set('n', '<leader>r',  vim.lsp.buf.rename, opts)
    end

    -- TODO: include this with the rest of the servers
    --[[nvim_lsp.sumneko_lua.setup {
      on_attach = on_attach,
      cmd = {"lua-language-server"},
      settings = {
        Lua = {
          runtime = {
            -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
            version = 'LuaJIT',
            -- Setup your lua path
            path = runtime_path,
          },
          diagnostics = {
            -- Get the language server to recognize the `vim` global
            globals = {'vim'},
          },
          workspace = {
            -- Make the server aware of Neovim runtime files
            library = vim.api.nvim_get_runtime_file("", true),
          },
          -- Do not send telemetry data containing a randomized but unique identifier
          telemetry = {
            enable = false,
          },
        },
      }
    }]]

    -- TODO: generate this list based on installed servers
    local servers = {
      "clangd",
      "rust_analyzer",
      "pyright",
      "zls"
    }

    local capabilities = require('cmp_nvim_lsp').default_capabilities()

    for _, lsp in ipairs(servers) do
      nvim_lsp[lsp].setup {
        on_attach = on_attach,
        capabilities = capabilities,
      }
    end
  end },

  'nvim-treesitter/nvim-treesitter-textobjects',
  'nvim-treesitter/playground',
  { 'nvim-treesitter/nvim-treesitter', build = ':TSUpdate', configure = function()
    require'nvim-treesitter.configs'.setup {
      -- ensure_installed = "all",
      auto_install = true,
      ignore_install = {},
      highlight = { enable = true },
      incremental_selection = { enable = true },
      indent = { enable = true },
      textobjects = {
        select = {
          enable = true,

          -- Automatically jump forward to textobj, similar to targets.vim
          lookahead = true,

          keymaps = {
            -- You can use the capture groups defined in textobjects.scm
            ["af"] = "@function.outer",
            ["if"] = "@function.inner",
            ["ac"] = "@class.outer",
            ["ic"] = "@class.inner",
          },
        },
      },
    }
  end },

  --[[{ 'lukas-reineke/indent-blankline.nvim', configure = function()
    vim.g.indentLine_char = '│'
    vim.g.indent_blankline_buftype_exclude = { 'terminal' }
    vim.g.indent_blankline_filetype_exclude = { 'help', 'dashboard' }
    vim.g.indent_blankline_show_current_context = true
    vim.g.indent_blankline_show_end_of_line = true
    vim.g.indent_blankline_show_trailing_blankline_indent = false
    -- vim.g.indent_blankline_space_char = '⋅'
    vim.g.indent_blankline_space_char = ' '
    vim.g.indent_blankline_use_treesitter = true
  end }]]

  -- Needs session config, see todo on vim-obsession
  -- also look into vim-startify
  -- 'glepnir/dashboard-nvim',

  -- Completion
  'hrsh7th/cmp-nvim-lsp',
  'hrsh7th/cmp-emoji',
  'hrsh7th/cmp-path',
  'hrsh7th/cmp-nvim-lua',
  'ray-x/cmp-treesitter',
  'f3fora/cmp-spell',
  { 'hrsh7th/nvim-cmp', configure = function()
    local cmp = require 'cmp'

    vim.o.completeopt = "menu,menuone,noselect"

    cmp.setup({
      snippet = {
        expand = function(args)
          vim.fn["vsnip#anonymous"](args.body)
        end,
      },
      mapping = cmp.mapping.preset.insert({
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<CR>'] = cmp.mapping.confirm({select = true}),
        ['<C-e>'] = cmp.mapping.close(), -- TODO: should this be esc?
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
      }),
      sources = {
        { name = 'nvim_lsp' },
        { name = 'path' },
        { name = 'nvim_lua' },
        { name = 'treesitter' },
        { name = 'emoji' },
        { name = 'spell' },
      }
    })

    cmp.setup.filetype({ 'markdown', 'help' }, {
      completion = {
        autocomplete = false
      }
    })
  end },

  -- a snippet engine is required for cmp
  'hrsh7th/cmp-vsnip',
  'hrsh7th/vim-vsnip',

  'ray-x/lsp_signature.nvim',

  -- Debugging
  -- 'jbyuki/one-small-step-for-vimkind',
  -- 'rcarriga/nvim-dap-ui',
  -- { 'mfussenegger/nvim-dap', configure = function()
  --   nnoremap <silent> <F5> :lua require'dap'.continue()<CR>
  --   nnoremap <silent> <F10> :lua require'dap'.step_over()<CR>
  --   nnoremap <silent> <F11> :lua require'dap'.step_into()<CR>
  --   nnoremap <silent> <leader>rr :lua require'dap'.step_out()<CR>
  --   nnoremap <silent> <leader>b :lua require'dap'.toggle_breakpoint()<CR>
  --   nnoremap <silent> <leader>B :lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>
  --   nnoremap <silent> <leader>lp :lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>
  --   nnoremap <silent> <leader>dr :lua require'dap'.repl.open()<CR>
  --   nnoremap <silent> <leader>dl :lua require'dap'.run_last()<CR>
  --   autocmd  FileType dap-repl lua require('dap.ext.autocompl').attach()
  --
  --   local dap = require"dap"
  --   dap.configurations.lua = {
  --     {
  --       type = 'nlua',
  --       request = 'attach',
  --       name = "Attach to running Neovim instance",
  --       host = '127.0.0.1',
  --       -- host = function()
  --       --   local value = vim.fn.input('Host [127.0.0.1]: ')
  --       --   if value ~= "" then
  --       --     return value
  --       --   end
  --       --   return '127.0.0.1'
  --       -- end,
  --       port = function()
  --         local val = tonumber(vim.fn.input('Port: '))
  --         assert(val, "Please provide a port number")
  --         return val
  --       end,
  --     }
  --   }
  --
  --   dap.adapters.nlua = function(callback, config)
  --     callback({ type = 'server', host = config.host, port = config.port })
  --   end
  --
  --   require("dapui").setup({
  --     icons = { expanded = "▾", collapsed = "▸" },
  --     mappings = {
  --       -- Use a table to apply multiple mappings
  --       expand = { "<CR>", "<2-LeftMouse>" },
  --       open = "o",
  --       remove = "d",
  --       edit = "e",
  --       repl = "r",
  --     },
  --     sidebar = {
  --       -- You can change the order of elements in the sidebar
  --       elements = {
  --         -- Provide as ID strings or tables with "id" and "size" keys
  --         {
  --           id = "scopes",
  --           size = 0.25, -- Can be float or integer > 1
  --         },
  --         { id = "breakpoints", size = 0.25 },
  --         { id = "stacks", size = 0.25 },
  --         { id = "watches", size = 00.25 },
  --       },
  --       size = 40,
  --       position = "left", -- Can be "left", "right", "top", "bottom"
  --     },
  --     tray = {
  --       elements = { "repl" },
  --       size = 10,
  --       position = "bottom", -- Can be "left", "right", "top", "bottom"
  --     },
  --     floating = {
  --       max_height = nil, -- These can be integers or a float between 0 and 1.
  --       max_width = nil, -- Floats will be treated as percentage of your screen.
  --       border = "single", -- Border style. Can be "single", "double" or "rounded"
  --       mappings = {
  --         close = { "q", "<Esc>" },
  --       },
  --     },
  --     windows = { indent = 1 },
  --   })
  --
  --   local dap, dapui = require('dap'), require('dapui')
  --   dap.listeners.after.event_initialized['dapui_config'] = function() dapui.open() end
  --   dap.listeners.before.event_terminated['dapui_config'] = function() dapui.close() end
  --   dap.listeners.before.event_exited['dapui_config'] = function() dapui.close() end
  -- end }

  { 'https://github.com/nvim-tree/nvim-tree.lua', configure = function()
    local api = require("nvim-tree.api")

    vim.keymap.set("n", "<Leader>n", api.tree.toggle, {})

    require("nvim-tree").setup({
      sync_root_with_cwd = true,
      disable_netrw = false,
      hijack_netrw = false,
    })
  end },

  'tpope/vim-eunuch',
  'tpope/vim-sleuth',

  -- TODO: load Session.vim if it exists, and store in ~/.local for security
  -- see https://dev.to/abdus/session-management-in-vim-2ekh
  -- and https://github.com/rmagatti/auto-session
  -- 'tpope/vim-obsession',

  -- Buffer Minaplion
  -- TODO: I haven't seen echodoc work in a wile, is it broken?
  { 'http://github.com/Shougo/echodoc.vim', configure = function()
    vim.g["echodoc#enable_at_startup"] = 1
    vim.g["echodoc#type"] = 'floating'
  end },

  'tpope/vim-commentary',
  'tpope/vim-surround',
  'tpope/vim-repeat',
  'tpope/vim-abolish',
  'junegunn/vim-easy-align',

  -- Auto pairs
  { 'cohama/lexima.vim', configure = [[
    " lexima doens't play nicely with todo lists
    autocmd filetype markdown let b:lexima_disabled=1
  ]] },

  -- { 'michaelb/sniprun', build = 'bash ./install.sh', configure = [[
  --   nmap <silent> <Leader>s  <Plug>SnipRun
  --   vmap <silent> <Leader>s  <Plug>SnipRun
  --   nmap <silent> <Leader>sc <Plug>SnipReset
  -- ]] },

  -- Code Coverage, maybe useful in the future
  -- 'retorillo/istanbul.vim',

-- Test driven development
  { 'vim-test/vim-test', configure = function()
    -- TODO: map <Leader>t to either TestNearest or TestFile
    -- Also configure the strategy
    vim.keymap.set("n", "<Leader>tn", "<cmd>TestNearest<CR>", {})
    vim.keymap.set("n", "<Leader>tf", "<cmd>TestFile<CR>", {})
    vim.keymap.set("n", "<Leader>ts", "<cmd>TestSuite<CR>", {})
  end },

  { 'neomake/neomake', configure = [[
    function AutoNMake()
      " Skip enabling neomake for PKGBUILD files
      if expand('%:t') != "PKGBUILD"
        call neomake#configure#automake('rw')
        " call neomake#configure#automake('rw', 1000)
      endif
    endfunction

    " TODO: More filetypes, a PKGBUILD checker would be great
    autocmd FileType sh call AutoNMake()

    " Configure neomake to generate svgs from dot files on write
    let g:neomake_dot_dot_maker = {
    \  'args': ['-O', '-Tsvg'],
    \  'errorformat': 'Error: %f: %m in line %l%s'
    \}
    let g:neomake_dot_enabled_makers=['dot']
  ]] },

  -- File Navigation
  { 'preservim/tagbar', configure = function()
    vim.g.tagbar_sort = 0
    -- TODO: use gO, but not in help and man
    vim.keymap.set('n', '<Leader>o', '<cmd>Tagbar<CR>', {})
  end },

  -- Filetype Plugins
  -- 'sheerun/vim-polyglot',
  -- override changes in polyglot and prompt when a swap file exists
  -- set shortmess-=A

  'rust-lang/rust.vim',
  'sirtaj/vim-openscad',

  -- 'dpelle/vim-LanguageTool', { 'on': [ 'LanguageToolCheck' ] },
  -- let g:languagetool_cmd='/usr/bin/languagetool'

  -- 'mike-hearn/base16-vim-lightline',
  -- let g:lightline = {}
  -- let g:lightline.colorscheme = 'base16_<%= $THEME | sed "s/-/_/g" %>'

  -- Look and Feel
  -- 'plasticboy/vim-markdown' " pretty markdown when editing,
  'psliwka/vim-smoothie',
  -- TODO: replace with galaxyline.nvim or lualine.nvim

  -- 'itchyny/lightline.vim',
  --" \   'tabline': {
  --" \     'left': [ [ 'buffers' ] ],
  --" \     'right': [ [ 'close' ] ]
  --" \   },
  --" \   'component_expand': {
  --" \     'buffers': 'lightline#bufferline#buffers'
  --" \   },
  --" \   'component_type': {
  --" \     'buffers': 'tabsel'
  --" \   },
  --set showtabline=0
  --" let g:lightline = {
  --" \   'colorscheme': 'base16_<%= $THEME | sed "s/-/_/g" %>'
  --" \ }
  --
  -- let g:lightline#bufferline#show_number = 2
  -- let g:lightline#bufferline#min_buffer_count = 2

  -- TODO: replace with barbar.nvim or nvim-bufferline.lua and add harpoon buffers only mode
  -- 'mengelbrecht/lightline-bufferline',
  'https://github.com/nvim-tree/nvim-web-devicons',
  -- 'junegunn/goyo.vim', { 'on': [ 'Goyo' ] },
  -- Just in case I have to write lisp

  -- 'luochen1990/rainbow', { 'on': [ 'RainbowToggleOn', 'RainbowToggle' ] },
  -- let g:rainbow_active = 0
  -- autocmd FileType scheme RainbowToggleOn


  -- Extra Tools
  -- { 'on': [ 'StartupTime' ] },
  'tweekmonster/startuptime.vim',
  -- 'iamcco/markdown-preview.nvim', {,
  --     \ 'do': { -> mkdp#util#install() },
  --     \ 'for': ['markdown', 'vim-plug']
  -- \ }
}

-- install_paq will no-op with minimal resoruce usage if paq is already installed
local load_paq = function(plugins)
  local install_path = vim.fn.stdpath('data') .. '/site/pack/paqs/start/paq-nvim'

  if not vim.uv.fs_access(install_path, 'R') then
    vim.fn.system({'git', 'clone', '--depth=1', 'https://github.com/savq/paq-nvim.git', install_path})
    vim.cmd 'packadd paq-nvim'
  end

  return require('paq')(plugins)
end

local configure_plugin = function(plugin)
  if type(plugin) ~= "table" then
    return
  end

  configure = plugin.configure
  if configure == nil then
    return
  end

  if type(configure) == "string" then
    vim.cmd(configure)
  else
    configure()
  end
end

load_paq(plugins)

-- Paq doesn't have the ability to run code when a plugin is loaded so emeulate
-- that ourselves. This way if we disable a plugin, we can know configuration
-- also needs to be disabled.
vim.tbl_map(configure_plugin, plugins)
