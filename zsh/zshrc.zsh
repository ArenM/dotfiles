# (
#     # This is in a subshell to silence the job started / stopped messages
#     echo
#     fortune | cowsay -nf tux &
# )

# Plugins
source $HOME/.zsh_plugins.sh

# Interaction
autoload -Uz compinit
if [ -d "$XDG_RUNTIME_DIR" ]; then
    # Use xdg_runtime_dir for cache because it should be cleared on reboot, and
    # only be writeable buy the current user
    compinit -d "$XDG_RUNTIME_DIR/zcompdump"
else
    compinit -D
fi

bindkey -v
bindkey "^[[A"    history-substring-search-up	# Up Arrow
bindkey "^[[B"    history-substring-search-down	# Down Arrow
bindkey "^[[1;5D" backward-word			# Crtl Left Arrow
bindkey "^[[1;5C" forward-word			# Crtl Right Arrow
bindkey "^[[H"    beginning-of-line		# Home
bindkey "^[[F"    end-of-line			# End

zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
setopt no_list_ambiguous

# History
export HISTFILE="$HOME/.zsh_history"
export HISTSIZE=50000
export SAVEHIST=50000
setopt HIST_IGNORE_DUPS
setopt sharehistory
setopt hist_expire_dups_first

# Enviroment
if [ "$PATHS" != "true" ]; then
    export PATH="$PATH:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/.npm-packages/bin"
    export PATHS="true"
fi

eval "$(direnv hook zsh)"

export EDITOR=nvim

# ripgrep congiguration wip / todo
# RIPGREP_CONFIG_PATH="$HOME/.config/ripgrep/config"

trap "set-theme-foot -d" USR1
trap "set-theme-foot -l" USR2

# Fix term when using ssh
# TODO: this should change based on the local terminal 
alias ssh="TERM=xterm-256color ssh"

# alias e="$EDITOR"
alias vi=nvim
alias ls='ls --color'
alias ll='ls -alh'
alias fcd='cd $(fd --type d | fzf)'
alias :q='exit'
if (( $+commands[yay] )); then
    alias yay="LESS=SR yay"
fi
if (( $+commands[paru] )); then
    alias pa="paru"
fi

sprunge() {
    curl -F 'sprunge=<-' http://sprunge.us
}

ix() {
    curl -F 'f:1=<-' ix.io
}

termbin() {
    nc termbin.com 9999
    }

# rename image
_ri() {
    if ! [ -e "$1" ]; then
        echo "usage: ri <image>"
        return 1
    fi

    imv "$1"

    old_name="${1%.*}"
    ext="${1##*.}"
    if [ "$old_name.$ext" != "$1" ]; then
        echo "ERROR: couldn't parse file name"
        return 1
    fi

    printf "rename to: "
    read -r name
    name="$name.$ext"
    if [ -e "$name" ]; then
        echo "Error: $name already exists"
        return 1
    fi

    if [ -n "$name" ]; then
        mv "$1" "$name"
    fi
}

ri() {
    while [ "$#" -gt 0 ]; do
        _ri "$1" || return 1
        shift
    done
}

if [[ "$TERM" = "foot-extra" ]]; then
    pid=$$
    (die-with $pid ncat -U "$XDG_RUNTIME_DIR/theme.sock" --recv-only &) 2> /dev/null
fi
