#! /bin/sh

if [ -z "$XDG_RUNTIME_DIR" ]; then
    echo "ERROR: \$XDG_RUNTIME_DIR must be st" >&2
    exit 1
fi

# Output the order to select players in seperated by commas.
player_order() {
    # players that are currently playing get highest priority 
    playerctl -a status --format="{{status}} {{playerName}}" |
        sed -n 's/Playing \(.*\)/\1/p' |
        tr '\n' ','

    # then tidal, which I typically use for music
    printf "tidal-hifi,%%any"
}

static() {
    # We're not already running, start a new process
    echo "$$" > "$XDG_RUNTIME_DIR/noise.lck"
    exec play -qn synth 15:00 brownnoise fade q 1 -0 15 vol 0.7
}

kill_static() {
    # There's another copy running, stop it
    kill "$(cat "$XDG_RUNTIME_DIR/noise.lck")"
}

toggle_music() {
    playerctl play-pause --player="$(player_order)"
    echo 0 > /tmp/play_count
}

have_playerctl() {
    playerctl status > /dev/null 2>&1
}

exec 4<> "$XDG_RUNTIME_DIR/noise.lck"

# If we're currently playing static, then stop
if ! flock -n 4; then
    kill_static
    exit
fi

if [ "$1" != 'static' ] && have_playerctl; then
    # If there is a playerctl client, use that
    toggle_music
else
    # Otherwise launch static player
    static
fi
