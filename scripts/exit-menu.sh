#!/usr/bin/env bash

# dmenu="bemenu --scrollbar autohide -s -n -w -c -l8 -M 40 -H 20"
dmenu="fuzzel -d"

menu() {
    cat <<EOF
 Suspend
⏻ Poweroff
 Lock Screen
 Cancel
EOF
}

confirm() {
    case "$(printf "No\nYes" | $dmenu -p "$1?")" in
        Yes) return 0 ;;
        *) return 1 ;;
    esac
}

case "$(menu | $dmenu)" in
    *"Suspend") systemctl suspend ;;
    *"Poweroff") confirm poweroff && systemctl poweroff ;;
    *"Lock Screen") swaylock -c 000000 & ;;
    *) exit ;;
esac
