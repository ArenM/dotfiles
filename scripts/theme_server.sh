#! /bin/sh

exec 3>"$XDG_RUNTIME_DIR/theme.lock"
flock -n 3 || exit 2

rm -f "$XDG_RUNTIME_DIR/theme.set"
rm -f "$XDG_RUNTIME_DIR/theme.sock"
mkfifo "$XDG_RUNTIME_DIR/theme.set"

ncat --keep-open --send-only --listen --unixsock "$XDG_RUNTIME_DIR/theme.sock" <> "$XDG_RUNTIME_DIR/theme.set"
