#! /usr/bin/env sh

alias 'esh=./vendor/esh/esh'
PATH="$PATH:$(dirname "$0")"
. base-env.sh

esh "$1"
